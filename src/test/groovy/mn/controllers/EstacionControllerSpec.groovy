package mn.controllers

import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpStatus
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.runtime.EmbeddedApplication
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import jakarta.inject.Inject
import mn.data.EstacionRepository
import mn.model.Estacion
import mn.model.PreciosDTO
import mn.services.ParserEstacionesService
import spock.lang.Specification

@MicronautTest(transactional = false)
class EstacionControllerSpec extends Specification {

    @Inject
    EmbeddedApplication<?> application

    @Inject
    ParserEstacionesService service

    @Inject
    EstacionRepository estacionRepository

    @Inject
    @Client("/")
    HttpClient httpClient

    void 'test estacion'() {
        when:
        service.updateEstacion(sample())

        and:
        def response = httpClient.toBlocking().exchange("/estacion/9068", Estacion)

        then:
        response.status()== HttpStatus.OK
        response.body().idProvincia==sample().IDProvincia

        cleanup:
        estacionRepository.deleteAll()
    }

    void 'test precios'() {
        when:
        service.updateEstacion(sample())
        service.insertPrecio(sample())

        and:
        def response = httpClient.toBlocking().exchange("/estacion/9068/precios", PreciosDTO)

        then:
        response.status()== HttpStatus.OK
        response.body().estacionId==sample().IDEESS as long
        response.body().precios.size()

        cleanup:
        estacionRepository.deleteAll()
    }

    void 'test location'() {
        when:
        service.updateEstacion(sample())
        service.insertPrecio(sample())

        and:
        def response = httpClient.toBlocking().retrieve(HttpRequest.GET("/estacion/location?lat=1.1&log=2.2"),List<Estacion>)

        then:
        response.size()==1
        response.first().horario==sample().Horario

        cleanup:
        estacionRepository.deleteAll()
    }


    Map sample(){
        [
            "% BioEtanol": "0,0",
            "% Éster metílico": "0,0",
            "C.P.": "50810",
            "Dirección": "CALLE COOPERATIVA, 2",
            "Horario": "L-D: 24H",
            "IDCCAA": "02",
            "IDEESS": "9068",
            "IDMunicipio": "8109",
            "IDProvincia": "50",
            "Latitud": "41,938750",
            "Localidad": "ONTINAR DE SALZ",
            "Longitud (WGS84)": "-0,757611",
            "Margen": "D",
            "Municipio": "Zuera",
            "Precio Biodiesel": "",
            "Precio Bioetanol": "",
            "Precio Gas Natural Comprimido": "",
            "Precio Gas Natural Licuado": "",
            "Precio Gases licuados del petróleo": "",
            "Precio Gasoleo A": "1,699",
            "Precio Gasoleo B": "1,255",
            "Precio Gasoleo Premium": "1,739",
            "Precio Gasolina 95 E10": "",
            "Precio Gasolina 95 E5": "1,799",
            "Precio Gasolina 95 E5 Premium": "",
            "Precio Gasolina 98 E10": "",
            "Precio Gasolina 98 E5": "",
            "Precio Hidrogeno": "",
            "Provincia": "ZARAGOZA",
            "Remisión": "dm",
            "Rótulo": "COOPERATIVA SAN ISIDRO",
            "Tipo Venta": "P"
        ]
    }
}
