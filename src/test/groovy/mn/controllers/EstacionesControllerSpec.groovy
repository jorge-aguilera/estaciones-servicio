package mn.controllers

import io.micronaut.http.HttpStatus
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.runtime.EmbeddedApplication
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import jakarta.inject.Inject
import mn.data.EstacionRepository
import mn.model.LocalidadDTO
import mn.model.Municipio
import mn.model.MunicipioDTO
import mn.model.Provincia
import mn.model.ProvinciaDTO
import mn.services.ParserEstacionesService
import reactor.core.publisher.Flux
import spock.lang.Specification

@MicronautTest(transactional = false)
class EstacionesControllerSpec extends Specification {

    @Inject
    EmbeddedApplication<?> application

    @Inject
    ParserEstacionesService service

    @Inject
    EstacionRepository estacionRepository

    @Inject
    @Client("/")
    HttpClient httpClient

    void 'test provincias'() {
        when:
        service.updateEstacion(sample())

        and:
        def response = httpClient.toBlocking().exchange("/geografia", List<Provincia>)

        then:
        response.status()== HttpStatus.OK
        response.body().size()==1
        response.body().first().idProvincia==sample().IDProvincia

        cleanup:
        estacionRepository.deleteAll()
    }

    void 'test provincia'() {
        when:
        service.updateEstacion(sample())

        and:
        def response = httpClient.toBlocking().exchange("/geografia/50", ProvinciaDTO)

        then:
        response.status()== HttpStatus.OK
        response.body().idProvincia==sample().IDProvincia
        response.body().municipios.size()==1
        response.body().municipios.first().idMunicipio==sample().IDMunicipio

        cleanup:
        estacionRepository.deleteAll()
    }

    void 'test municipio'() {
        when:
        service.updateEstacion(sample())

        and:
        def response = httpClient.toBlocking().exchange("/geografia/50/8109", MunicipioDTO)

        then:
        response.status()== HttpStatus.OK
        response.body().idMunicipio==sample().IDMunicipio
        response.body().localidades.size()
        response.body().localidades.first().localidad=='ONTINAR DE SALZ'

        cleanup:
        estacionRepository.deleteAll()
    }

    void 'test localidad'() {
        when:
        service.updateEstacion(sample())

        and:
        def response = httpClient.toBlocking().exchange("/geografia/50/8109/ONTINAR%20DE%20SALZ", LocalidadDTO)

        then:
        response.status()== HttpStatus.OK
        response.body().localidad==sample().Localidad
        response.body().estaciones.size()==1
        response.body().estaciones.first().cp=='50810'

        cleanup:
        estacionRepository.deleteAll()
    }

    Map sample(){
        [
            "% BioEtanol": "0,0",
            "% Éster metílico": "0,0",
            "C.P.": "50810",
            "Dirección": "CALLE COOPERATIVA, 2",
            "Horario": "L-D: 24H",
            "IDCCAA": "02",
            "IDEESS": "9068",
            "IDMunicipio": "8109",
            "IDProvincia": "50",
            "Latitud": "41,938750",
            "Localidad": "ONTINAR DE SALZ",
            "Longitud (WGS84)": "-0,757611",
            "Margen": "D",
            "Municipio": "Zuera",
            "Precio Biodiesel": "",
            "Precio Bioetanol": "",
            "Precio Gas Natural Comprimido": "",
            "Precio Gas Natural Licuado": "",
            "Precio Gases licuados del petróleo": "",
            "Precio Gasoleo A": "1,699",
            "Precio Gasoleo B": "1,255",
            "Precio Gasoleo Premium": "1,739",
            "Precio Gasolina 95 E10": "",
            "Precio Gasolina 95 E5": "1,799",
            "Precio Gasolina 95 E5 Premium": "",
            "Precio Gasolina 98 E10": "",
            "Precio Gasolina 98 E5": "",
            "Precio Hidrogeno": "",
            "Provincia": "ZARAGOZA",
            "Remisión": "dm",
            "Rótulo": "COOPERATIVA SAN ISIDRO",
            "Tipo Venta": "P"
        ]
    }
}
