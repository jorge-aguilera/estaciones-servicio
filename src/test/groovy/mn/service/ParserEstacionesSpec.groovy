package mn.service

import io.micronaut.runtime.EmbeddedApplication
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import jakarta.inject.Inject
import mn.data.EstacionRepository
import mn.data.PrecioRepository
import mn.services.ParserEstacionesService
import spock.lang.Specification

@MicronautTest
class ParserEstacionesSpec extends Specification {

    @Inject
    EmbeddedApplication<?> application

    @Inject
    ParserEstacionesService service

    @Inject
    EstacionRepository estacionRepository

    @Inject
    PrecioRepository precioRepository

    void 'test it works'() {
        when:
        service.doIt().block()

        then:
        estacionRepository.count()

        then:
        precioRepository.count()
    }

}
