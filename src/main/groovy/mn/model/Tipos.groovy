package mn.model

/**
 * @author : jorge <jorge.aguilera@seqera.io>
 * */
class Tipos {

    def static carburantes = [
            "Precio Biodiesel"                  : "Biodiesel",
            "Precio Bioetanol"                  : "Bioetanol",
            "Precio Gas Natural Comprimido"     : "GasNaturalComprimido",
            "Precio Gas Natural Licuado"        : "GasNaturalLicuado",
            "Precio Gases licuados del petróleo": "GasLicuadoPetroleo",
            "Precio Gasoleo A"                  : "GasoleoA",
            "Precio Gasoleo B"                  : "GasoleoB",
            "Precio Gasoleo Premium"            : "GasoleoPremium",
            "Precio Gasolina 95 E10"            : "Gasolina95E10",
            "Precio Gasolina 95 E5"             : "Gasolina95E5",
            "Precio Gasolina 95 E5 Premium"     : "Gasolina95E5Premium",
            "Precio Gasolina 98 E10"            : "Gasolina98E10",
            "Precio Gasolina 98 E5"             : "Gasolina98E5",
            "Precio Hidrogeno"                  : "Hidrogeno",
    ]


}
