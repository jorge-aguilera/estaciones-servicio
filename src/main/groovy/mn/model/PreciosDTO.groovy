package mn.model

/**
 * @author : jorge <jorge.aguilera@seqera.io>
 * */
class PreciosDTO {

    long estacionId
    Set<Precio> precios

}
