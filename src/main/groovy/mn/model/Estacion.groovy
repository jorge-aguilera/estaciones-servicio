package mn.model

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

/**
 * @author : jorge <jorge.aguilera@seqera.io>
 * */
@Entity
class Estacion {

    @Id
    Long id

    float latitud
    float longitud
    String direccion
    String cp
    String horario

    String idCCAA

    String idProvincia
    String provincia

    String municipio
    String idMunicipio

    String localidad

    String margen
    String remision
    String rotulo
    String tipoVenta

}
