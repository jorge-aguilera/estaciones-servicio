package mn.model

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import java.time.Instant

/**
 * @author : jorge <jorge.aguilera@seqera.io>
 * */
@Entity
class Precio {

    @Id
    @GeneratedValue
    long id

    long estacionId
    String tipo
    Date fecha
    Float precio

}
