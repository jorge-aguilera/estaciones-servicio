package mn.model

import io.micronaut.core.annotation.Introspected

/**
 * @author : jorge <jorge.aguilera@seqera.io>
 * */
@Introspected
class Municipio {
    String idMunicipio
    String municipio

}
