package mn.controllers

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Post
import mn.services.ParserEstacionesService
import reactor.core.publisher.Mono

/**
 * @author : jorge <jorge.aguilera@seqera.io>
 * */
@Controller("/admin")
class AdminController {

    ParserEstacionesService parserEstacionesService

    AdminController(ParserEstacionesService parserEstacionesService) {
        this.parserEstacionesService = parserEstacionesService
    }

    @Post("/run")
    Mono<Long> run(){
        parserEstacionesService.doIt()
    }

}
