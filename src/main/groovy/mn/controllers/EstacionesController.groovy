package mn.controllers

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import mn.data.EstacionRepository
import mn.model.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

import java.util.function.Function

/**
 * @author : jorge <jorge.aguilera@seqera.io>
 * */
@Controller("/geografia")
class EstacionesController {

    EstacionRepository estacionRepository

    EstacionesController(EstacionRepository estacionRepository) {
        this.estacionRepository = estacionRepository
    }

    Function<Provincia, ProvinciaDTO> transformProvincia = (Provincia p) ->{
        def muninicipios = estacionRepository.findMunicipios(p.idProvincia)
        new ProvinciaDTO(idProvincia: p.idProvincia, provincia: p.provincia, municipios: muninicipios)
    }

    Function<Municipio, MunicipioDTO> transformMunicipio = (Municipio m) ->{
        def localidades = estacionRepository.findLocalidades(m.idMunicipio)
        new MunicipioDTO(idMunicipio: m.idMunicipio, municipio: m.municipio, localidades: localidades)
    }

    Function<Localidad, LocalidadDTO> transformLocalidad = (Localidad l) ->{
        def estaciones = estacionRepository.findEstaciones(l.localidad)
        new LocalidadDTO(localidad: l.localidad, estaciones: estaciones)
    }

    @Get("/")
    Flux<Provincia> getProvincias() {
        Flux.fromIterable(estacionRepository.findProvincias().stream().collect())
    }

    @Get("/{id}")
    Mono<ProvinciaDTO> getProvincia(String id) {
        Mono.fromCallable({
            transformProvincia.apply(estacionRepository.findProvincia(id).orElseThrow())
        })
    }

    @Get("/{provincia}/{municipio}")
    Mono<MunicipioDTO> getMunicipio(String provincia, String municipio) {
        Mono.fromCallable({
            transformMunicipio.apply(estacionRepository.findMunicipio(municipio).orElseThrow())
        })
    }

    @Get("/{provincia}/{municipio}/{localidad}")
    Mono<LocalidadDTO> getMunicipio(String provincia, String municipio, String localidad) {
        Mono.fromCallable({
            transformLocalidad.apply(estacionRepository.findLocalidad(localidad).orElseThrow())
        })
    }


}
