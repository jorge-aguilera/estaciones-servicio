package mn.controllers

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import mn.data.EstacionRepository
import mn.data.PrecioRepository
import mn.model.*
import mn.services.LocationService
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

import java.util.function.Function

/**
 * @author : jorge <jorge.aguilera@seqera.io>
 * */
@Controller("/estacion")
class EstacionController {

    EstacionRepository estacionRepository

    PrecioRepository precioRepository

    LocationService locationService

    EstacionController(EstacionRepository estacionRepository,
                       PrecioRepository precioRepository,
                       LocationService locationService) {
        this.estacionRepository = estacionRepository
        this.precioRepository = precioRepository
        this.locationService = locationService
    }

    Function<Estacion, PreciosDTO> transformEstacion = (Estacion e) ->{
        def precios = precioRepository.findByEstacionId(e.id)
        new PreciosDTO(precios:precios, estacionId: e.id)
    }

    @Get("/{id}")
    Mono<Estacion> getEstacion(long id) {
        Mono.fromCallable{
            estacionRepository.findById(id).orElseThrow()
        }
    }

    @Get("/{id}/precios")
    Mono<PreciosDTO> getPrecios(long id) {
        Mono.fromCallable{
            def estacion = estacionRepository.findById(id).orElseThrow()
            transformEstacion.apply(estacion)
        }
    }

    @Get("/location{?lat,log,tipo,marca}")
    Flux<Estacion> nearTo(Optional<Float> lat , Optional<Float> log, Optional<String> tipo, Optional<String> marca){
        locationService.nearTo(lat.get(), log.get(), tipo, marca, 3)
    }

}
