package mn.minetur

import io.micronaut.http.annotation.Get
import io.micronaut.http.client.annotation.Client

/**
 * @author : jorge <jorge.aguilera@seqera.io>
 * */
@Client('https://sedeaplicaciones.minetur.gob.es/ServiciosRESTCarburantes/PreciosCarburantes/EstacionesTerrestres')
interface EstacionesTerrestresClient {

    @Get("/")
    Map estaciones()

}