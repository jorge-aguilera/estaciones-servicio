package mn.services

import jakarta.inject.Singleton
import mn.data.EstacionRepository
import mn.data.PrecioRepository
import mn.model.Estacion
import mn.model.Precio
import reactor.core.publisher.Flux

@Singleton
class LocationService {

    EstacionRepository estacionRepository

    PrecioRepository precioRepository

    LocationService(EstacionRepository estacionRepository, PrecioRepository precioRepository) {
        this.estacionRepository = estacionRepository
        this.precioRepository = precioRepository
    }

    Flux<Estacion>nearTo(float lat , float log, Optional<String> tipo, Optional<String> marca, int size){

        Flux.create {sink ->
            List<Estacion> all = estacionRepository.findAll() as List<Estacion>
            all.sort { eess ->
                metersTo(eess.latitud, eess.longitud, lat, log)
            }
            List<Estacion> tmp = []
            if (marca.isPresent()) {
                marca = marca.get().toLowerCase()
                tmp.addAll all.findAll { eess ->
                    eess.rotulo.toLowerCase().indexOf(marca) != -1
                }
            }
            if (tmp.size()) {
                all = tmp
            }

            int count=0
            all.each {estacion->
                if( tipo.isPresent() ){
                    Optional<Precio> precio = precioRepository.findByEstacionIdAndTipo(estacion.id, tipo.get())
                    if( precio.isEmpty() )
                        return
                }
                if( count++ < size)
                    sink.next(estacion)
            }
            sink.complete()
        }

    }

    static float metersTo(float lat1, float lng1, float lat2, float lng2) {
        double radioTierra = 6371;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);
        double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2) * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
        double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));
        double meters = radioTierra * va2;
        meters as float;
    }

}
