package mn.services

import io.micronaut.scheduling.annotation.Scheduled
import jakarta.inject.Singleton
import mn.data.EstacionRepository
import mn.data.PrecioRepository
import mn.minetur.EstacionesTerrestresClient
import mn.model.Estacion
import mn.model.Precio
import mn.model.Tipos
import reactor.core.publisher.Mono

import javax.transaction.Transactional
import java.time.Instant

/**
 * @author : jorge <jorge.aguilera@seqera.io>
 * */
@Singleton
class ParserEstacionesService {


    EstacionRepository estacionRepository
    PrecioRepository precioRepository
    EstacionesTerrestresClient estacionesTerrestresClient

    ParserEstacionesService(EstacionRepository estacionRepository,
                            PrecioRepository precioRepository,
                            EstacionesTerrestresClient estacionesTerrestresClient) {
        this.estacionRepository = estacionRepository
        this.precioRepository = precioRepository
        this.estacionesTerrestresClient = estacionesTerrestresClient
    }

    @Scheduled(cron = "0 0 12 * * ?")
    void everyDay(){
        doIt().subscribe()
    }

    Mono<Long> doIt() {
        Mono.fromCallable({

            // por ahora no mantenemos historico de precios
            precioRepository.deleteAll()

            Map last = estacionesTerrestresClient.estaciones()
            (last.ListaEESSPrecio as List<Map>).stream().parallel().each { e ->
                updateEstacion(e)
                insertPrecio(e)
            }
            estacionRepository.count()
        })
    }

    /*
{
            "% BioEtanol": "0,0",
            "% Éster metílico": "0,0",
            "C.P.": "50810",
            "Dirección": "CALLE COOPERATIVA, 2",
            "Horario": "L-D: 24H",
            "IDCCAA": "02",
            "IDEESS": "9068",
            "IDMunicipio": "8109",
            "IDProvincia": "50",
            "Latitud": "41,938750",
            "Localidad": "ONTINAR DE SALZ",
            "Longitud (WGS84)": "-0,757611",
            "Margen": "D",
            "Municipio": "Zuera",
            "Precio Biodiesel": "",
            "Precio Bioetanol": "",
            "Precio Gas Natural Comprimido": "",
            "Precio Gas Natural Licuado": "",
            "Precio Gases licuados del petróleo": "",
            "Precio Gasoleo A": "1,699",
            "Precio Gasoleo B": "1,255",
            "Precio Gasoleo Premium": "1,739",
            "Precio Gasolina 95 E10": "",
            "Precio Gasolina 95 E5": "1,799",
            "Precio Gasolina 95 E5 Premium": "",
            "Precio Gasolina 98 E10": "",
            "Precio Gasolina 98 E5": "",
            "Precio Hidrogeno": "",
            "Provincia": "ZARAGOZA",
            "Remisión": "dm",
            "Rótulo": "COOPERATIVA SAN ISIDRO",
            "Tipo Venta": "P"
        }
     */

    @Transactional
    long updateEstacion(Map data) {
        Estacion estacion = estacionRepository.findById(data.IDEESS as Long)
                .orElse(
                        estacionRepository.save(new Estacion(
                                id: data.IDEESS as Long,
                                latitud: data."Latitud".replace(',', '.') as float,
                                longitud: data."Longitud (WGS84)".replace(',', '.') as float,
                                direccion: data."Dirección",
                                cp: data."C.P.",
                                horario: data."Horario",
                                idCCAA: data."IDCCAA",
                                idMunicipio: data."IDMunicipio",
                                idProvincia: data."IDProvincia",
                                localidad: data."Localidad",
                                margen: data."Margen",
                                municipio: data."Municipio",
                                provincia: data."Provincia",
                                remision: data."Remisión",
                                rotulo: data."Rótulo",
                                tipoVenta: data."Tipo Venta"
                        )))
        return estacion.id
    }

    @Transactional
    void insertPrecio(Map data) {
        Tipos.carburantes.each{ kv ->
            Precio add = new Precio(
                    estacionId: data.IDEESS as Long,
                    fecha: new Date(),
                    tipo: kv.value,
                    precio: ('0'+data[kv.key].replace(',','.')) as float
            )
            precioRepository.save(add)
        }
    }

}
