package mn.data

import io.micronaut.data.jdbc.annotation.JdbcRepository
import io.micronaut.data.model.query.builder.sql.Dialect
import io.micronaut.data.repository.CrudRepository
import mn.model.Precio

/**
 * @author : jorge <jorge.aguilera@seqera.io>
 * */
@JdbcRepository(dialect = Dialect.POSTGRES)
interface PrecioRepository extends CrudRepository<Precio, Long> {

    Set<Precio>findByEstacionId(long estacionId)

    Optional<Precio>findByEstacionIdAndTipo(long estacionId, String tipo)
}
