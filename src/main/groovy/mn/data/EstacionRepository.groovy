package mn.data

import io.micronaut.data.annotation.Query
import io.micronaut.data.jdbc.annotation.JdbcRepository
import io.micronaut.data.model.query.builder.sql.Dialect
import io.micronaut.data.repository.CrudRepository
import mn.model.Estacion
import mn.model.Localidad
import mn.model.Municipio
import mn.model.Provincia
import reactor.core.publisher.Flux

/**
 * @author : jorge <jorge.aguilera@seqera.io>
 * */
@JdbcRepository(dialect = Dialect.POSTGRES)
interface EstacionRepository extends CrudRepository<Estacion, Long>{

    @Query("select distinct id_provincia, provincia FROM estacion")
    Set<Provincia>findProvincias()

    @Query("select distinct id_provincia, provincia FROM estacion where id_provincia=cast(:provincia as text)")
    Optional<Provincia> findProvincia(String provincia)

    @Query("select distinct id_municipio, municipio FROM estacion where id_provincia=cast(:provincia as text)")
    Set<Municipio>findMunicipios(String provincia)

    @Query("select distinct id_municipio, municipio FROM estacion where id_municipio=cast(:municipio as text)")
    Optional<Municipio>findMunicipio(String municipio)

    @Query("select distinct localidad FROM estacion where id_municipio=cast(:municipio as text)")
    Set<Localidad>findLocalidades(String municipio)

    @Query("select distinct localidad FROM estacion where localidad=cast(:localidad as text)")
    Optional<Localidad>findLocalidad(String localidad)

    @Query("select * FROM estacion where localidad=cast(:localidad as text)")
    Set<Estacion>findEstaciones(String localidad)

}