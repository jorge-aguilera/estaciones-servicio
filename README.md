## Estaciones de Servicio

## Open data

precios de carburante obtenidos de 
https://sedeaplicaciones.minetur.gob.es/ServiciosRESTCarburantes/PreciosCarburantes/EstacionesTerrestres

## REST

Servicio REST con las estaciones y ultimos precios importados

Organizacion de las estaciones por Provincia/Municipio/Localidad

- /geografia/{provincia}/{municipio}/{localidad}

Detalles de una Estacion de Servio

- /estacion/{id}